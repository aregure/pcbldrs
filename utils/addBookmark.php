<?php
	session_start();
	$name = $_GET['name'];
	$url = $_GET['prevURL'];
?>

<html>

	<head>
		<meta http-equiv="refresh" content="0;url=<?php echo $url; ?>" />
	</head>

	<?php
		if (!isset($_SESSION['entries'])) {
			$entries = array();
			$_SESSION['entries'] = $entries;
		}
		
		array_push($_SESSION['entries'], [$url, $name]);
	?>

</html>