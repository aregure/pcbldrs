<html>
    <head>
        <title>PCBLDRS - Your One and Only PC Build Guide</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="./styles/style.css">
        <link rel="stylesheet" href="./styles/fonts.css">
        <link rel="stylesheet" href="./styles/typography.css">
        <script src="scripts/effects.js"></script>
    </head>
    <body onload="fade()">
        <div class="navi">
            <a href="./index.php"><img src="./images/PCBLDRS-logo-web.png" class="logo"></a>
            <ul>
                <li><a href="./guides.php">Guides</a></li>
                <li><a href="#">Discussions</a></li>
            </ul>
        </div>
        
        <div class="top hidden">
            <div class="upper">
                <h1>Learn how to build a PC today.</h1>
                <p>
                    As PC building enthusiasts, we at PCBLDRS are committed to providing
                    the most comprehensive guide to building your very own PC.
                </p>
                <input type="button" class="learnMoreBtn"
                    onclick="document.getElementById('spacer').scrollIntoView();"
                    value="LEARN MORE">
            </div>
        </div>
        <div id="spacer"></div>
        <div class="main">
            <div id="hdr" class="hidden">
                <h1>Why PCBLDRS?</h1>
                <p id="hint">Hover on each image to zoom in</p>
            </div>
            <div class="row">
                <div class="column hidden">
                    <div class="innerTextRight">
                        <h2>Step-by-step build guides</h2>
                        <p>
                            PCBLDRS offers a clear, concise, and easy-to-follow build guide for your specific PC build.
                            Simply specify your PC configuration, and the right build guide will be served for you.
                        </p>
                    </div>
                    <img class="pic" src="./images/pc02c.png">
                </div>
                <div class="column hidden">
                    <img class="pic" src="./images/pc04c.png">
                    <div class="innerTextLeft">
                        <h2>Community discussions</h2>
                        <p>
                            Stuck on something? Ask a question in our community forums! Countless community members are passionate
                            in the art of PC building, and are ready to help you in your PC building journey. They can also offer
							build suggestions for your specific use case.
                        </p>
                        <p>
                            If you are an experienced PC builder, you can offer help and advice to novice builders. We always love it when enthusiasts offer solid advice and helpful critique.
                        </p>
                    </div>
                </div>
                <div class="column hidden">
                    <div class="innerTextRight">
                        <h2>Build suggestions</h2>
                        <p>
                            PCBLDRS also offers PC part suggestions based on your needs and budget.
                            Simply follow the introductory guide in the <a href="./guides.php">Guides</a> section, and you can see suggestions
                            for your specific use case (suggestions may change with future PC component releases).
                        </p>
                    </div>
                    <img class="pic" src="./images/pc05c.png">
                </div>
            </div>

            <div class="cta">
                So, what are you waiting for?<br><a href="./signup.php" style="text-decoration: none;">Sign up</a> now!
            </div>
        </div>

        <div class="footer">
            Copyright 2020 PCBLDRS<br>
            Made with love by John Paul Alegre<br>
            Everything from scratch, no external libs <br>
        </div>
    </body>
</html>