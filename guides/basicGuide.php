<?php
    session_start();
?>

<!doctype html>
<html>
    <head>
        <title>Basic PC Build Guide | PCBLDRS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../styles/style2.css">
        <link rel="stylesheet" href="../styles/fonts.css">
        <link rel="stylesheet" href="../styles/guideStyle.css">
    </head>
    <body">
        <div class="navi">
            <a href="../index.php"><img src="../images/PCBLDRS-logo-web.png" class="logo"></a>
            <ul>
                <li><a href="../guides.php">Guides</a></li>
                <li><a href="#">Discussions</a></li>
            </ul>
        </div>
        
        <div class="top">
            <div class="upper">
                <h1>Basic PC Build Guide</h1>
            </div>
        </div>
        <div class="main">
            <div id="intro">
                <p>
                    This is a guide for you to get started with the fundamentals of PC building. The guide covers basic assembly
                    of the hardware components into a whole PC, up to the installation of the operating system.
                </p>
            </div>
            <div class="column">
                <a name="step1"><h2>Step 1: Preparations</h2></a>
                <div class="addButton">
                    <a id="addBookmark" href="../utils/addBookmark.php?name=Basic%20PC%20Build%20Guide%20%2F%20Step%201&prevURL=%2E%2E%2Fguides%2FbasicGuide.php%23step1">Bookmark this step</a>
                </div>
                <img class="pic" src="../images/pc02c2.png">
                <div class="innerText">
                    <p>
                        Before we start building a PC, we need to do some preparations.
                    </p>
                    <p>
                        First, we will need a few tools. Make sure you have the following:
                        <ul>
                            <li>A sturdy table for your working area</li>
                            <li>A philips-head screwdriver</li>
                            <li>Some zip ties for better cable management</li>
                            <li>A tube of thermal paste (if you bought an aftermarket cooler to use with your CPU)</li>
                            <li>An anti-static wrist band (which has a wire connected to ground)
                                to prevent electrostatic discharge from damaging your components</li>
                        </ul>
                    </p>
                    <p>
                        If you do not have an anti-static wrist band, you can touch a bare piece of metal to remove any
                        unwanted static electricity from your body, before touching any components.
                    </p>
                    <p>
                        Also, make sure that you have all your PC components ready.
                    </p>
                </div>
            </div>
            <div class="column">
                <a name="step2"><h2>Step 2: Prepare the motherboard</h2></a>
                <div class="addButton">
                    <a id="addBookmark" href="../utils/addBookmark.php?name=Basic%20PC%20Build%20Guide%20%2F%20Step%202&prevURL=%2E%2E%2Fguides%2FbasicGuide.php%23step2">Bookmark this step</a>
                </div>
                <img class="pic" src="../images/mobo.jpg">
                <div class="innerText">
                    <p>
                        Carefully unbox the motherboard to avoid dropping it, and to avoid damaging the box, since it is almost
                        always required should you send the part back for replacements in case of a defect.
                    </p>
                    <p>
                        After unboxing the motherboard and removing the anti-static bag, place it on top of its box. The box will
                        serve as your working platform for the motherboard. It also helps if you place the anti-static bag on top
                        of the box before placing the motherboard on top.
                    </p>
                </div>
            </div>
            <div class="column">
                <a name="step3"><h2>Step 3: Unbox the CPU</h2></a>
                <div class="addButton">
                    <a id="addBookmark" href="../utils/addBookmark.php?name=Basic%20PC%20Build%20Guide%20%2F%20Step%203&prevURL=%2E%2E%2Fguides%2FbasicGuide.php%23step3">Bookmark this step</a>
                </div>
                <div class="innerText">
                    <p>
                        Unbox the CPU (again, carefully). The CPU, is the square chip with a metal piece on top. It is the brains
                        of your PC. It should be inside a plastic casing together with a sticker stating the model of the CPU.
                    </p>
                    <p>
                        Open the plastic casing, and carefully hold the CPU without touching any of the gold pins (if it's an AMD
                        CPU) or pads (if it's an Intel CPU).
                    </p>
                </div>
            </div>
            <div class="column">
                <a name="step4"><h2>Step 4: Install the CPU on the motherboard</h2></a>
                <div class="addButton">
                    <a id="addBookmark" href="../utils/addBookmark.php?name=Basic%20PC%20Build%20Guide%20%2F%20Step%204&prevURL=%2E%2E%2Fguides%2FbasicGuide.php%23step4">Bookmark this step</a>
                </div>
                <img class="pic" src="../images/cpu.jpg">
                <div class="innerText">
                    <p>
                        On the motherboard, there is a socket for the CPU to go into. It matches with the pins or pads on the CPU.
                        Follow the steps below according to the system you have chosen:
                    </p>
                    <h3>For AMD systems:</h3>
                    <p>
                        Gently push down on the retention arm (the lever-like piece) and away from the socket to unlatch it, then
                        lift it up. Now carefully hold the CPU by the sides without touching any pins.
                    </p>
                    <p>
                        On the CPU, there should be a gold triangle on one of the corners, that indicates the position that it
                        goes into the socket, and on the motherboard, there should be a small dot that also indicates the CPU
                        position. Align the triangle and the dot, then gently place the CPU on the socket. It should drop into
                        place without any insertion force.
                    </p>
                    <p>
                        Once the CPU is inserted correctly into the socket, gently lower the retention arm and latch it into
                        place. Your CPU is now installed on the motherboard.
                    </p>
                    <p>
                        <b>WARNING</b>: Make sure to insert the CPU in the correct position. Otherwise, the CPU pins might get
                        bent and render the CPU broken or unusable.
                    </p>
                    <h3>For Intel systems:</h3>
                    <p>
                        Gently push down on the retention arm (the lever-like piece) and away from the socket to unlatch it, then
                        lift it up. The bracket and the socket cover should lift up along with the retention arm. Do not remove
                        the socket cover yet. Carefully hold the CPU by the sides without touching the gold pads on the bottom.
                    </p>
                    <p>
                        On the CPU, you should see two notches, one on each side, which should correspond with two protrusions on
                        the socket on the motherboard. There is also a gold triangle on the corner which aligns with a small dot
                        on the socket. Align the CPU with the socket by matching the position of the gold triangle with the small
                        dot on the socket, and gently place the CPU on the socket. The two notches should fit with the protruding
                        bits on the socket. Refer to the illustration above for a visual guide.
                    </p>
                    <p>
                        Once the CPU is properly placed on the socket, lower the retention arm. The bracket and the socket cover
                        should lower along with the retention arm. Latch the retention arm into place to secure the CPU on the
                        socket. The CPU cover should pop off after latching the retention arm. Your CPU is now installed on the
                        motherboard. Put the CPU cover on the motherboard's box for safekeeping. You will need to put back the
                        CPU cover on the motherboard should you return the motherboard for replacement, because motherboard
                        manufacturers will not honor your motherboard's warranty if the CPU cover is missing.
                    </p>
                    <p>
                        <b>WARNING:</b> Make sure to insert the CPU in the correct position and avoid touching the pins on the
                        socket to avoid breaking them.
                    </p>
                </div>
            </div>
            <div class="column">
                <a name="step5"><h2>Step 5: Install the CPU cooler</h2></a>
                <div class="addButton">
                    <a id="addBookmark" href="../utils/addBookmark.php?name=Basic%20PC%20Build%20Guide%20%2F%20Step%205&prevURL=%2E%2E%2Fguides%2FbasicGuide.php%23step5">Bookmark this step</a>
                </div>
                <img class="pic" src="../images/cooler.jpg">
                <div class="innerText">
                    <p>
                        The CPU cooler is what keeps your CPU from overheating when it's in use. Therefore, it's important that
                        you install it correctly.
                    </p>
                    <p>
                        If you are using an all-in-one water cooler, you might want to install the cooler after the radiator has been installed inside the case.
                    </p>
                    <p>
                        If the cooler is included inside the CPU's box, it almost always comes with thermal paste (which is
                        typically of grey color) already applied on the heatsink (which is a huge chunk of metal that dissipates
                        the heat from the CPU), at the point where it makes contact with the CPU. The thermal paste is what
                        ensures good heat transfer between the CPU and the heatsink. Be careful not to touch the thermal paste or
                        accidentally spread it on other surfaces.
                    </p>
                    <p>
                        Otherwise, if you bought an aftermarket cooler (i.e. not included with the CPU), in most cases, there is
                        no thermal paste preapplied. In that case, you will need to apply the thermal paste yourself.
                    </p>
                    <p>
                        To apply thermal paste, grab your tube of thermal paste, open it, and squeeze out a small amount onto the
                        CPU. A small bead the size of a pea should be enough.
                    </p>
                    <p>
                        After making sure that you have thermal paste applied, you may now install the cooler onto the
                        motherboard. Refer to the CPU's installation manual (if you're using the included cooler), or the cooler's
                        installation manual (if you're using an aftermarket cooler) for detailed installation instructions. Don't
                        forget to connect the cooler's fan to the CPU fan header on the motherboard (refer to the motherboard's
                        manual for connection guides).
                    </p>
                </div>
            </div>
            <div class="column">
                <a name="step6"><h2>Step 6: Install the memory (RAM)</h2></a>
                <div class="addButton">
                    <a id="addBookmark" href="../utils/addBookmark.php?name=Basic%20PC%20Build%20Guide%20%2F%20Step%206&prevURL=%2E%2E%2Fguides%2FbasicGuide.php%23step6">Bookmark this step</a>
                </div>
                <img class="pic" src="../images/ram.jpg">
                <div class="innerText">
                    <p>
                        Unbox the RAM module and install it on the motherboard's RAM slot. Open the latches on each side of
                        the RAM slot on the motherboard. Keep in mind that the RAM module only goes in one way onto the slot.
                    </p>
                    <p>
                        Align the notch on the RAM module with the notch on the RAM slot. Insert it into the RAM slot and press
                        on each side until both sides click into place.
                    </p>
                    <p>
                        If you have multiple RAM modules, you almost always need to install them in dual-channel mode. The
                        motherboard's instruction manual has a guide on how to install the RAM modules in dual-channel mode.
                    </p>
                </div>
            </div>
            <div class="column">
                <a name="step7"><h2>Step 7: Install the M.2 storage drive</h2></a>
                <div class="addButton">
                    <a id="addBookmark" href="../utils/addBookmark.php?name=Basic%20PC%20Build%20Guide%20%2F%20Step%207&prevURL=%2E%2E%2Fguides%2FbasicGuide.php%23step7">Bookmark this step</a>
                </div>
                <img class="pic" src="../images/m2c.png">
                <div class="innerText">
                    <p>
                        If you are using an M.2 storage device as your primary storage drive, now is the perfect time to
                        install it. Otherwise, you can proceed with the next step.
                    </p>
                    <p>
                        First up, install the M.2 drive screw standoff (which is included in the motherboard box, and will be the
                        place you will screw the drive into) on the motherboard. Make sure to screw it on the appropriate
                        mounting hole for the size of your drive. You don't need to do this if the standoff has been preinstalled.
                    </p>
                    <p>
                        Next, insert the M.2 drive into the motherboard's M.2 drive slot. Keep in mind that it has notches
                        such that it only goes in one way. Also make sure that you are inserting into the right slot. Some M.2
                        slots only interface via SATA and some only interface with NVMe, so you need to consult your motherboard's
                        After inserting the drive into the slot, screw it in place using the provided screw into the standoff
                        that you mounted on the motherboard.
                    </p>
                    <p>
                        Now your M.2 drive is successfully installed on the motherboard, and all the installations on the
                        motherboard are complete. You can set aside the motherboard combo for now so we can work on the case.
                    </p>
                </div>
            </div>
            <div class="column">
                <a name="step8"><h2>Step 8: Prepare the case</h2></a>
                <div class="addButton">
                    <a id="addBookmark" href="../utils/addBookmark.php?name=Basic%20PC%20Build%20Guide%20%2F%20Step%208&prevURL=%2E%2E%2Fguides%2FbasicGuide.php%23step8">Bookmark this step</a>
                </div>
                <img class="pic" src="../images/case.jpg">
                <div class="innerText">
                    <p>
                        Gently take the case from its box, and remove both side panels. Lay the case in such a way that you can
                        easily work on it while installing the components to it.
                    </p>
                    <p>
                        If you have auxiliary fans that go into the case, install them into the case now. Refer to the case's
                        instruction manual for a guide on how to install case fans.
                    </p>
                    <p>
                        You might want to sort the cable by their purpose, whether they are case fan cables, or front panel
                        cables, so you can easily work with them later.
                    </p>
                    <p>
                        Install the motherboard's I/O shield (the part that covers the area around the motherboard's ports)
                        by aligning it in the correct position with the case and firmly pushing on it until it snaps into place.
                    </p>
                    <p>
                        Check if the case has motherboard standoffs (the brass screws with screw holes for the motherboard)
                        are preinstalled. If they are not, then you need to install them first so you can mount your motherboard
                        inside the case. The standoffs should be provided with the case. Refer to the case's instruction manual
                        for installing the standoffs. The general rule is that the positions of the standoffs should align with
                        the screw holes on the motherboard. After ensuring that there are standoffs in the case, you can now
                        install the motherboard.
                    </p>
                    <p>
                        If you are using an all-in-one water cooler, now is the perfect time to mount the radiator inside the
                        case. Consult the cooler's installation guide for instructions.
                    </p>
                </div>
            </div>
            <div class="column">
                <a name="step9"><h2>Step 9: Install the motherboard assembly into the case</h2></a>
                <div class="addButton">
                    <a id="addBookmark" href="../utils/addBookmark.php?name=Basic%20PC%20Build%20Guide%20%2F%20Step%209&prevURL=%2E%2E%2Fguides%2FbasicGuide.php%23step9">Bookmark this step</a>
                </div>
                <div class="innerText">
                    <p>
                        Hold the motherboard, preferably by the CPU cooler (don't worry, it's secured in place) or by the side
                        if you don't have a cooler installed yet. Place it inside the case, aligning the motherboard's screw
                        holes with the standoffs in the case. Make sure that the ports on the motherboard line up with the holes
                        on the I/O shield.
                    </p>
                    <p>
                        After the motherboard is aligned correctly, screw it into place using the motherboard screws provided
                        with the case. Fasten all screws (typically 9 for ATX motherboards, and 6 for mATX motherboards) to ensure
                        that it is secured in place.
                    </p>
                    <p>
                        Plug the case fans and front panel ports to the motherboard. For better cable management, it is highly
                        suggested that you route the cables along the side where the cables are hidden from view, then route
                        the cables through the case holes around the motherboard. Refer to the motherboard's manual for guides
                        on where to plug the case fans and front panel I/O. Cable zip ties will also help in managing cables and
                        making them more organized.
                    </p>
                    <p>
                        Also at this point, if you are using an all-in-one water cooler, you can now install the cooler onto the
                        motherboard. Don't forget to apply thermal paste if it isn't applied already!
                    </p>
                    <p>
                        After plugging all case fans and front panel I/O ports, you can now proceed with installing the power
                        supply.
                    </p>
                </div>
            </div>
            <div class="column">
                <a name="step10"><h2>Step 10: Install the power supply unit (PSU)</h2></a>
                <div class="addButton">
                    <a id="addBookmark" href="../utils/addBookmark.php?name=Basic%20PC%20Build%20Guide%20%2F%20Step%2010&prevURL=%2E%2E%2Fguides%2FbasicGuide.php%23step10">Bookmark this step</a>
                </div>
                <img class="pic" src="../images/psu.jpg">
                <div class="innerText">
                    <p>
                        Take the power supply unit from its box (along with the necessary cables if you have a modular PSU).
                        Route the cables along the side such that the cables are hidden from view, for better cable
                        management. Then route the cables through the appropriate hole inside the case such that it reaches
                        the appropriate motherboard connector.
                    </p>
                    <p>
                        Mount the power supply to the rear of the case by fastening the provided screws through the power supply's
                        screw holes. You typically want the power supply mounted with the fan facing down if your case has venting
                        holes at the bottom of the case for proper ventilation. Make sure that all screws are fastened to secure
                        the PSU in place.
                    </p>
                    <p>
                        After which, plug the power cables into the motherboard. First is the 24-pin power cable, which supplies
                        power to the motherboard (except the CPU socket). Then plug the 8-pin CPU power cable, which will
                        deliver power to the CPU. If you have a graphics card that needs external power, you will need additional
                        power cables. For example, if your graphics card requires a 6-pin PCI-express power connector, you will
                        need a 6-pin power connector. These connectors are typically labeled "PCI-e". Refer to the motherboard
                        manual for details on where to plug the power connectors.
                    </p>
                </div>
            </div>
            <div class="column">
                <a name="step11"><h2>Step 11: Install the drives</h2></a>
                <div class="addButton">
                    <a id="addBookmark" href="../utils/addBookmark.php?name=Basic%20PC%20Build%20Guide%20%2F%20Step%2011&prevURL=%2E%2E%2Fguides%2FbasicGuide.php%23step11">Bookmark this step</a>
                </div>
                <img class="pic" src="../images/storage2.png">
                <div class="innerText">
                    <p>
                        If you have secondary storage, such as a high-capacity solid-state or hard drive, or if you did not opt
                        for an M.2 drive, you need to install either a solid-state drive or a hard disk drive.
                    </p>
                    <p>
                        Inside your case, there are drive bays where you can mount your drives. Smaller 2.5-inch drives are
                        typically mounted using a bracket inside the case. The most important thing is that the drive is mounted
                        properly and securely. Consult the case's instruction guide for details on how to install a drive.
                    </p>
                    <p>
                        After the drive is mounted inside the case, it's time to connect the SATA power and data cables. The
                        SATA power cable originates from the power cable and should be labeled "SATA" or something similar. Plug
                        it into the drive. It only goes in one way, so do not forcefully jam it if it does not go in. For the
                        SATA data cable, there is a connector on the motherboard, and a connector on the drive itself. Route the
                        cable (remember good cable management practice) and plug the cable in both the motherboard and the drive.
                        Again, refer to the motherboard manual if you are having trouble finding the SATA data connector. After
                        this, your drive should be successfully installed.
                    </p>
                </div>
            </div>
            <div class="column">
                <a name="step12"><h2>Step 12: Install the graphics card (GPU)</h2></a>
                <div class="addButton">
                    <a id="addBookmark" href="../utils/addBookmark.php?name=Basic%20PC%20Build%20Guide%20%2F%20Step%2012&prevURL=%2E%2E%2Fguides%2FbasicGuide.php%23step12">Bookmark this step</a>
                </div>
                <img class="pic" src="../images/gpu.jpg">
                <div class="innerText">
                    <p>
                        If you opted for a graphics card, naturally, you will need to install it.
                    </p>
                    <p>
                        To install the graphics card, you will first need to free a few slot covers on the rear of the case,
                        depending on the slot configuration of the graphics card (whether it's single-slot, dual-slot, etc.).
                        Your case's manual should have instructions on how to remove these slot covers.
                    </p>
                    <p>
                        After freeing some slot covers, install the graphics card into the motherboard by inserting it in such a
                        was that the graphics card's connector is going inside the motherboard's PCI-express slot. Make sure to
                        pick the topmost PCI-e slot, since that is typically the fastest PCI-express slot on the motherboard.
                    </p>
                    <p>
                        After installing the graphics card into the motherboard, you need to give it power, if it needs an
                        external PCI-e connector. Connect the PCI-e power cable to the graphics card's power connector.
                    </p>
                </div>
            </div>
            <div class="column">
                <a name="step13"><h2>Step 13: Tidy up</h2></a>
                <div class="addButton">
                    <a id="addBookmark" href="../utils/addBookmark.php?name=Basic%20PC%20Build%20Guide%20%2F%20Step%2013&prevURL=%2E%2E%2Fguides%2FbasicGuide.php%23step13">Bookmark this step</a>
                </div>
                <div class="innerText">
                    <p>
                        If you don't want the inside of your PC to look like a jungle of wires, you need to do cable management.
                        As pointed out above, the holes around the motherboard, the other side of your case, and some zip ties
                        are your best friends when it comes to cable management. Good cable management also means that the inside
                        of the PC has excellent airflow, and it translates to lower temperatures inside the system.
                    </p>
                    <p>
                        Route the cables appropriately, and in such a way that they are hidden from view. That way, the cables
                        are organized and managed, both aesthetically and functionally.
                    </p>
                    <p>
                        If you're happy with your cable management, you can now close the side panels. The PC is now assembled!
                    </p>
                    <p>
                        Connect the PC to a monitor, keyboard, and mouse to prepare for the installation of an operating system.
                        Don't forget to connect the PC's power cable!
                    </p>
                </div>
            </div>
            <div class="column">
                <a name="step14"><h2>Step 14: Install the operating system</h2></a>
                <div class="addButton">
                    <a id="addBookmark" href="../utils/addBookmark.php?name=Basic%20PC%20Build%20Guide%20%2F%20Step%2014&prevURL=%2E%2E%2Fguides%2FbasicGuide.php%23step14">Bookmark this step</a>
                </div>
                <img class="pic" src="../images/winInstallc.png">
                <div class="innerText">
                    <p>
                        Start by turning on the PC with the power button. If it does not turn on, check the switch on the power
                        supply and that it is not in the 'off' position. Otherwise, double check all of your connections, as well
                        as the input of the monitor. 
                    </p>
                    <p>
                        While the PC is booting, repeatedly press [Delete] or any other key that the motherboard specifies, until
                        your PC enters the BIOS menu.
                    </p>
                    <p>
                        Once your PC is on the BIOS menu, you will need to configure the primary boot device. It is typically a
                        list, with the device on top being the first one to boot from. You need to set the first boot device to be
                        your boot drive. If you are using an M.2 drive as your primary drive, then it should be set to your M.2
                        storage device. Otherwise, it should be set to your SATA drive.
                    </p>
                    <p>
                        After setting your primary boot device, you may now proceed to installing your operating system from a USB
                        drive. You can use software like Rufus (if you are installing a GNU/Linux distro) or the Windows USB Media
                        Creation tool (which will download the latest version of Windows and copy it to your bootable USB drive),
                        to create an installation media for your operating system.
                    </p>
                    <p>
                        Insert the bootable USB drive into your PC and boot off of it by selecting the USB device from your BIOS
                        boot menu. The PC should now boot to an operating system install guide, and you just need to follow the
                        prompts in the installer to install your operating system. You don't need to worry about losing data if
                        you are using a brand-new drive with no data on it.
                    </p>
                    <p>
                        After following the installer prompts and waiting for the install process to finish, the operating system
                        should be installed. The prompt will most likely tell you to restart at this point. Most motherboards
                        nowadays now know if an operating system is installed on the primary boot drive and will not
                        erroneously attempt to boot from the installation USB for the second time.
                    </p>
                    <p>
                        At this point, your PC should now boot into your new operating system.
                    </p>
                </div>
            </div>
            <div class="column">
                <a name="step15"><h2>Step 15: Install the essential software</h2></a>
                <div class="addButton">
                    <a id="addBookmark" href="../utils/addBookmark.php?name=Basic%20PC%20Build%20Guide%20%2F%20Step%2015&prevURL=%2E%2E%2Fguides%2FbasicGuide.php%23step15">Bookmark this step</a>
                </div>
                <div class="innerText">
                    <p>
                        Once you've booted into your freshly-installed operating system and performing the initial setup,
                        you will need to install the device drivers for your motherboard, the onboard audio, and the graphics
                        device. You can find details about this in the relevant manufacturer's website. After downloading the
                        drivers, just install them by double-clicking on its setup package. If the setup program tells you to
                        restart after installing a driver, just restart before proceeding to install another driver, to avoid
                        any problems.
                    </p>
                    <p>
                        After installing drivers, you can now install additional software, such as your preferred web browser,
                        media player, office programs, games, etc.
                    </p>
                    <p>
                        Congratulations, you have built your very own PC! If you have any problems, feel free to read the
                        relevant section again, in case you missed something, or consult our community forums for assistance.
                    </p>
                </div>
            </div>
        </div>

        <div class="footer">
            Copyright 2020 PCBLDRS<br>
            Made with love by John Paul Alegre<br>
            Everything from scratch, no external libs<br>
        </div>
    </body>
</html>