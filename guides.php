<?php
    session_start();
?>

<!doctype html>
<html>
    <head>
        <title>Guides | PCBLDRS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="./styles/style2.css">
        <link rel="stylesheet" href="./styles/fonts.css">
    </head>
    <body>
        <div class="navi">
            <a href="./index.php"><img src="./images/PCBLDRS-logo-web.png" class="logo"></a>
            <ul>
                <li><a href="./guides.php">Guides</a></li>
                <li><a href="#">Discussions</a></li>
            </ul>
        </div>
        
        <div class="top">
            <div class="upper">
                <h1>Guides</h1>
            </div>
        </div>
        <div class="main">
            <img id="notice" src="./images/pcmr.jpg">
            <h1>PC Build Guides</h1>
            <div class="innerText">
                <p>
                    These are guides to help you in building your very own PC
                </p>
                <ul>
                    <li><a href="./guides/basicGuide.php">Basic PC Build Guide</a></li>
                </ul>
            </div>
            <h1>Bookmarks</h1>
            <div class="innerText">
                <?php
                    if (!isset($_SESSION['entries'])) {
                        echo "<p> No bookmarks saved for this session. </p>";
                    } else {
                        echo "<p> Your bookmarked guides/steps for this session are here: </p>";
                        echo "<ul>";
                        foreach($_SESSION['entries'] as $entry) {
                            echo "<li> <a href='" . $entry[0] . "'>" . $entry[1] . "</a></li>";
                        }
                        echo "</ul>";
                        echo "<a href='./utils/destroy.php'>Clear all bookmarks</a>";
                    }
                ?>
            </div>
        </div>

        <div class="footer">
            Copyright 2020 PCBLDRS<br>
            Made with love by John Paul Alegre<br>
            Everything from scratch, no external libs <br>
        </div>
    </body>
</html>